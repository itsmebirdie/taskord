<?php

namespace App\View\Components\Labels;

use Illuminate\View\Component;

class Beta extends Component
{
    public function render()
    {
        return view('components.labels.beta');
    }
}
