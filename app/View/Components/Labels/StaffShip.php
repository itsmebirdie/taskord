<?php

namespace App\View\Components\Labels;

use Illuminate\View\Component;

class StaffShip extends Component
{
    public function render()
    {
        return view('components.labels.staff-ship');
    }
}
