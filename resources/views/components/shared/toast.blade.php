<div class="position-fixed top-0 end-0" style="z-index: 5">
    <div id="liveToast" class="toast hide m-3" role="alert" aria-live="assertive" aria-atomic="true">
        <div class="toast-body d-flex justify-content-between align-items-center py-3">
            <div class="d-flex align-items-center">
                <div id="toast-icon" class="me-2"></div>
                <div id="toast-body"></div>
            </div>
            <button type="button" class="btn-close btn-sm" data-bs-dismiss="toast" aria-label="Close"></button>
        </div>
    </div>
</div>
